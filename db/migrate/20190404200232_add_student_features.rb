class AddStudentFeatures < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :student_family_name, :string
  	add_column :users, :student_first_given_name, :string
  	add_column :users, :student_second_given_name, :string
  	add_column :users, :student_gender, :string
  	add_column :users, :student_date_of_birth, :date
  	add_column :users, :student_year_entering, :integer
  	add_column :users, :student_phone_number, :integer
  	add_column :users, :caregiver_contact_number, :integer
  	add_column :users, :student_aboriginality, :integer
  	add_column :users, :student_lote, :boolean
  	add_column :users, :student_lote_first_language, :string
  	add_column :users, :student_lote_second_language, :string
  end
end
