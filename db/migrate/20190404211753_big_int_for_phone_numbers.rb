class BigIntForPhoneNumbers < ActiveRecord::Migration[5.1]
  def change
  	change_column :users, :caregiver_contact_number, :bigint
  	change_column :users, :student_aboriginality, :bigint
  	change_column :users, :student_phone_number, :bigint
  end
end
