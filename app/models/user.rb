class User < ApplicationRecord
	before_save { self.email = email.downcase }
	validates :name, presence: true, length: { maximum: 50 }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum: 255 },
									format: { with: VALID_EMAIL_REGEX },
									uniqueness: { case_sensitive: false }
	# has_secure_password
	#validates :password, presence: true, length: { minimum: 6 }
	validates :student_family_name, presence: true, length: { maximum: 20 }
	validates :student_first_given_name, presence: true, length: { maximum: 20 }
	validates :student_second_given_name, length: { maximum: 20 }
	validates :student_gender, presence: true, length: { maximum: 20 }
	validates :student_date_of_birth, presence: true, length: { maximum: 20 }
	validates :student_year_entering, presence: true, length: { maximum: 20 }
	validates :student_phone_number, length: { maximum: 20 }
	validates :caregiver_contact_number, presence: true, length: { maximum: 20 }
	validates :student_aboriginality, presence: true, length: { maximum: 20 }
	validates :student_lote, length: { maximum: 20 }
	validates :student_lote_first_language, presence: true, if: :lote?, length: { maximum: 20 }
	validates :student_lote_second_language, length: { maximum: 20 }

	def lote?
		student_lote == true
	end
end
