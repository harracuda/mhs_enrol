class UsersController < ApplicationController
  def show
  	@user = User.find(params[:id])
  end

  def school
  	@users = User.all
  end

  def new
  	@user = User.new
  end

  def create
  	@user = User.new(user_params)
  	if @user.save
  		flash[:success] = "Thank you for enrolling!"
  		redirect_to @user
  	else
  		render 'new'
  	end
  end

  private

  def user_params
  	params.require(:user).permit(:name, :email, :password, :password_confirmation, :student_family_name,
  								:student_first_given_name, :student_second_given_name, :student_gender,
  								:student_date_of_birth, :student_phone_number, :caregiver_contact_number,
  								:student_aboriginality, :student_lote, :student_lote_first_language,
  								:student_lote_second_language, :student_year_entering)
  end

end
